package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder(
{ "id", "username", "name", "email", "avatar", "dob", "gender", "proflePresent", "date", "updatedDate" })
public class UserDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4549359339343452956L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("username")
	private String username;

	@JsonProperty("name")
	private String name;

	@JsonProperty("email")
	private String email;

	@JsonProperty("avatar")
	private String avatar;

	@JsonProperty("dob")
	private Date dob;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("profilePresent")
	private boolean profilePresent;

	@JsonProperty("date")
	private Date date;

	@JsonProperty("updatedDate")
	private Date updatedDate;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}

	@JsonProperty("name")
	public String getName()
	{
		return name;
	}

	@JsonProperty("name")
	public void setName(String name)
	{
		this.name = name;
	}

	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email)
	{
		this.email = email;
	}

	@JsonProperty("avatar")
	public String getAvatar()
	{
		return avatar;
	}

	@JsonProperty("avatar")
	public void setAvatar(String avatar)
	{
		this.avatar = avatar;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("dob")
	public Date getDob()
	{
		return dob;
	}

	@JsonProperty("dob")
	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	@JsonProperty("gender")
	public String getGender()
	{
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	@JsonProperty("profilePresent")
	public boolean isProfilePresent()
	{
		return profilePresent;
	}

	@JsonProperty("profilePresent")
	public void setProfilePresent(boolean profilePresent)
	{
		this.profilePresent = profilePresent;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

}