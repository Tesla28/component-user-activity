package com.quot.api.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.quot.api.service.UserActivityService;

@RestController
@RequestMapping("/quot/api/user-activity")
public class UserActivityController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserActivityController.class);
	
	@Autowired
	private UserActivityService userActivityService;
	
	
	/**
	 * 
	 * API for logging user activity
	 * 
	 * @author Akshay
	 * @param activity
	 * @param token
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(path = "/add/activity", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity addActivity(@RequestParam(name = "activity", required = true) String activity, @RequestHeader("Authorization") String token)
	{
		LOGGER.trace("in addActivity controller method... activity : " + activity);
		Map<String, Object> responseMap = null;
		
		responseMap = userActivityService.addActivityService(activity, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			return new ResponseEntity(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			return new ResponseEntity(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("response from getUser : " + responseMap);
		return new ResponseEntity(responseMap, HttpStatus.CREATED);
	}
}
