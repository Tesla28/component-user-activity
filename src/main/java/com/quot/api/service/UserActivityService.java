package com.quot.api.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.dto.UserActivityDTO;
import com.quot.api.dto.UserDTO;
import com.quot.api.helper.UserActivityServiceHelper;
import com.quot.api.repository.UserActivityRepository;

@Service
public class UserActivityService
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserActivityService.class);

	@Autowired
	private AuthConsumer authConsumer;
	
	@Autowired
	private UserActivityServiceHelper userActivityServiceHelper;
	
	@Autowired
	private UserActivityRepository userActivityRepository;
	
	/**
	 * 
	 * Service method to add user activity to database
	 * 
	 * @author Akshay
	 * @param activity
	 * @param token
	 * @return
	 */
	public Map<String, Object> addActivityService(String activity, String token)
	{
		LOGGER.trace("in addActivityService method");
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				UserDTO user = authConsumer.getCurrentUserFromToken(token);
				LOGGER.trace("logged in user data : " + user.toString());
				
				String userId = user.getId();
				LOGGER.trace("userId : " + userId);
				
				UserActivityDTO userActivity = new UserActivityDTO();
				UUID uuid = UUID.randomUUID();
				userActivity.setId("activity_" + uuid.toString());
				userActivity.setUserId(userId);
				userActivity.setDate(userActivityServiceHelper.parseFormattedDate());
				userActivity.setActivity(activity);
				LOGGER.trace("user activity data : " + userActivity.toString());
				userActivityRepository.save(userActivity);
				LOGGER.trace("User activity data saved");
				
				responseMap.put("statusCode", 201);
				responseMap.put("statusMessage", "Created");
				responseMap.put("message", "User activity data successfully saved");
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in addActivityService service : " + err);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in addActivityService service : " + err);
			responseMap.put("statusCode", 500);
			return responseMap;
		}
		
		LOGGER.trace("Response from addActivityService... responseMap : " + responseMap);
		return responseMap;
	}

}
