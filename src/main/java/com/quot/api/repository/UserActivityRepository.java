package com.quot.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.UserActivityDTO;

@Repository
public interface UserActivityRepository extends MongoRepository<UserActivityDTO, Integer>
{

}
