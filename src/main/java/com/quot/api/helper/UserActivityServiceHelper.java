package com.quot.api.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserActivityServiceHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserActivityServiceHelper.class);
	
	/**
	 * Helper method to get the formatted date from the input date
	 * 
	 * @author Akshay
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public Date parseFormattedDate() throws ParseException
	{
		Date currentDate = new Date();
		LOGGER.trace("current unformatted date : " + currentDate);
		String datePattern = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
		String parsedDateString = sdf.format(currentDate);
		Date parsedDate = sdf.parse(parsedDateString);
		LOGGER.trace("current formatted date : " + parsedDate);
		return parsedDate;
	}
}
