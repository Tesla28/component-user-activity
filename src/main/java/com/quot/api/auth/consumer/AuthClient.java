package com.quot.api.auth.consumer;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "COMPONENT-AUTH", url = "localhost:7011")
public interface AuthClient
{
	@RequestMapping(path = "/quot/api/auth/validate", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> validateToken(@RequestParam("token") String token);
	
	@RequestMapping(path = "/quot/api/auth/getUserFromToken", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getCurrentUserFromToken(@RequestHeader("token") String token,
			@RequestHeader("Authorization") String authToken);
}
