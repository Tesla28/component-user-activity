package com.quot.api.auth.consumer;


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.dto.UserDTO;

@Service
public class AuthConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthConsumer.class);
	
	@Autowired
	private AuthClient authClient;
	
	
	/**
	 * 
	 * Service method to call component-auth's method to check if requested token String is valid or not
	 * 
	 * @author Akshay
	 * @param token
	 * @return boolean isTokenValid
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes" })
	public boolean isTokenValid(String token) throws Exception
	{
		LOGGER.trace("in isValidToken method");
		boolean isTokenValid = false;
		ResponseEntity response = null;
		try 
		{
			response = authClient.validateToken(token);
			LOGGER.trace("response from component auth : " + response);
			if(response.getStatusCode().equals(HttpStatus.UNAUTHORIZED))
			{
				LOGGER.trace("Unauthorized response for token");
				isTokenValid = false;
			}
			else 
			{
				LOGGER.trace("Authorized response for token");
				isTokenValid = true;
			}
		}
		catch(Exception err) 
		{
			isTokenValid = false;
			LOGGER.error("Error in isTokenValid : " + err.getMessage());
		}
		
		LOGGER.trace("Response of isTokenValid : " + isTokenValid);
		return isTokenValid;
	}
	
	
	/**
	 * 
	 * Component auth consumer method to get current logged in user details
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 */
	public UserDTO getCurrentUserFromToken(String token)
	{
		LOGGER.trace("in getCurrentUserFromToken method");
		ResponseEntity<Map<String, Object>> response = null;
		UserDTO userResponse = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try
		{
			response = authClient.getCurrentUserFromToken(token.substring(7), token);
			LOGGER.trace("Response from component-auth :  " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("the response status code is 200 OK... user : " + response.getBody().get("user"));
				userResponse = mapper.convertValue(response.getBody().get("user"), UserDTO.class);
			}
		}
		catch(Exception err)
		{
			userResponse = null;
			LOGGER.error("Error getCurrentUserFromToken : " + err);
		}
		
		LOGGER.trace("response from getCurrentUserFromToken : " + userResponse.toString());
		return userResponse;
	}
	

}
