FROM openjdk:11
ADD target/docker-component-user-activity.jar docker-component-user-activity.jar
EXPOSE 7071
ENTRYPOINT ["java", "-jar", "docker-component-user-activity.jar"]